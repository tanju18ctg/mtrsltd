<?php

use App\Http\Controllers\Admin\AboutUsController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\ContactController;
use App\Http\Controllers\Admin\GallaryController;
use App\Http\Controllers\Admin\ManagementController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\ProjectController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\SocialLinkController;
use App\Http\Controllers\Frontend\ClientReviewController;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', [FrontendController::class, 'index']);
Route::get('/products', [FrontendController::class, 'productIndex']);
Route::get('/our-gallary', [FrontendController::class, 'gallaryIndex']);
Route::get('/about-us', [FrontendController::class, 'aboutIndex']);
Route::get('/contact-us', [FrontendController::class, 'contactIndex']);



// Admin Routes//
Route::get('/admin', [AdminController::class, 'index']);
Route::get('/admin/products', [ProductController::class, 'index'])->name('products');
Route::post('/product-store', [ProductController::class, 'store'])->name('product-store');
Route::get('/admin/product-edit/{product_id}', [ProductController::class, 'edit']);
Route::post('/update-product', [ProductController::class, 'update'])->name('update-product');
Route::get('/admin/product-delete/{product_id}', [ProductController::class, 'delete']);
Route::get('/admin/dashboard', [AdminController::class, 'dashboardIndex'])->name('dashboard');


Route::get('/admin/slider', [SliderController::class, 'index'])->name('sliders');
Route::post('/slider-store', [SliderController::class, 'store'])->name('slider-store');
Route::get('/admin/slider-edit/{id}', [SliderController::class, 'edit']);
Route::post('/slider-update', [SliderController::class, 'update'])->name('slider-update');
Route::get('/admin/slider-delete/{id}', [SliderController::class, 'delete']);

Route::get('/admin/gallary', [GallaryController::class, 'index'])->name('gallary');
Route::post('/gallary-store', [GallaryController::class, 'store'])->name('gallary-store');
Route::get('/admin/gallary-edit/{id}', [GallaryController::class, 'edit']);
Route::post('/update-gallary', [GallaryController::class, 'update'])->name('gallary-update');
Route::get('/admin/gallary-delete/{id}', [GallaryController::class, 'delete']);


Route::get('/admin/contact', [ContactController::class, 'index'])->name('contact');
Route::post('/contact-store', [ContactController::class, 'store'])->name('contact-store');
Route::get('/admin/contact-edit/{id}', [ContactController::class, 'edit']);
Route::post('/contact-update', [ContactController::class, 'update'])->name('contact-update');
Route::get('/admin/contact-delete/{id}', [ContactController::class, 'delete']);


Route::post('/client-store', [ClientReviewController::class, 'store'])->name('client-store');
Route::get('/client-message', [ClientReviewController::class, 'index'])->name('client-message');


Route::get('/social-link', [SocialLinkController::class, 'index'])->name('social-link');
Route::post('/social-link-store', [SocialLinkController::class, 'store'])->name('social-link-store');
Route::get('/social-link-edit/{id}', [SocialLinkController::class, 'edit']);
Route::post('/social-link-update', [SocialLinkController::class, 'update'])->name('social-link-update');



Route::get('/admin/about-us', [AboutUsController::class, 'index'])->name('about-us');
Route::post('/about-store', [AboutUsController::class, 'store'])->name('about-store');
Route::get('/admin/about-edit/{id}', [AboutUsController::class, 'edit']);
Route::post('/about-update', [AboutUsController::class, 'update'])->name('about-update');
Route::get('/admin/about-delete/{id}', [AboutUsController::class, 'delete']);

Route::get('/admin/management', [ManagementController::class, 'index'])->name('management');
Route::post('/management-store', [ManagementController::class, 'store'])->name('management-store');
Route::get('/admin/management-edit/{id}', [ManagementController::class, 'edit']);
Route::post('/management-update', [ManagementController::class, 'update'])->name('management-update');
Route::get('/admin/management-delete/{id}', [ManagementController::class, 'delete']);

// Projects
Route::get('/admin/mtrs-lubricants', [ProjectController::class, 'index'])->name('mtrs-lubricants');
Route::post('/admin/mtrs-lubricants-store', [ProjectController::class, 'store'])->name('mtrs-lubricants-store');
Route::get('/admin/mtrs-lubricants-edit/{id}', [ProjectController::class, 'edit']);
Route::post('/admin/mtrs-lubricants-update', [ProjectController::class, 'update'])->name('mtrs-lubricants-update');
Route::get('/admin/mtrs-lubricants-delete/{id}', [ProjectController::class, 'delete']);


Route::get('/admin/mtrs-multimedia', [ProjectController::class, 'MultimediaIndex'])->name('mtrs-multimedia');
Route::post('/admin/mtrs-multimedia-store', [ProjectController::class, 'multimediaStore'])->name('mtrs-multimedia-store');
Route::get('/admin/mtrs-multimedia-edit/{id}', [ProjectController::class, 'multimediaEdit']);
Route::post('/admin/mtrs-multimedia-update', [ProjectController::class, 'multimediaUpdate'])->name('mtrs-multimedia-update');
Route::get('/admin/mtrs-multimedia-delete/{id}', [ProjectController::class, 'multimediaDelete']);


Route::get('/admin/mtrs-cable', [ProjectController::class, 'CableIndex'])->name('mtrs-cable');
Route::post('/admin/mtrs-cable-store', [ProjectController::class, 'cableStore'])->name('mtrs-cable-store');
Route::get('/admin/mtrs-cable-edit/{id}', [ProjectController::class, 'cableEdit']);
Route::post('/admin/mtrs-cable-update', [ProjectController::class, 'cableUpdate'])->name('mtrs-cable-update');
Route::get('/admin/mtrs-cable-delete/{id}', [ProjectController::class, 'cableDelete']);

Route::get('/admin/mtrs-courier', [ProjectController::class, 'Courierindex'])->name('mtrs-courier');
Route::post('/admin/mtrs-courier-store', [ProjectController::class, 'courierStore'])->name('mtrs-courier-store');
Route::get('/admin/mtrs-courier-edit/{id}', [ProjectController::class, 'courierEdit']);
Route::post('/admin/mtrs-courier-update', [ProjectController::class, 'courierUpdate'])->name('mtrs-courier-update');
Route::get('/admin/mtrs-courier-delete/{id}', [ProjectController::class, 'courierDelete']);

Route::get('/admin/mtrs-food', [ProjectController::class, 'foodIndex'])->name('mtrs-food');
Route::post('/admin/mtrs-food-store', [ProjectController::class, 'foodStore'])->name('mtrs-food-store');
Route::get('/admin/mtrs-food-edit/{id}', [ProjectController::class, 'foodEdit']);
Route::post('/admin/mtrs-food-update', [ProjectController::class, 'foodUpdate'])->name('mtrs-food-update');
Route::get('/admin/mtrs-food-delete/{id}', [ProjectController::class, 'foodDelete']);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
