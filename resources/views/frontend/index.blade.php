<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">

    <title>Welcome to MTRS Ltd</title>

    <!-- CSS FILES -->
    <link rel="preconnect" href="https://fonts.googleapis.com">

    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

    <link href="https://fonts.googleapis.com/css2?family=DM+Sans:wght@400;500;700&display=swap" rel="stylesheet">

    <link href="{{ asset('frontend') }}/css/bootstrap.min.css" rel="stylesheet">

    <link href="{{ asset('frontend') }}/css/bootstrap-icons.css" rel="stylesheet">

    <link href="{{ asset('frontend') }}/css/templatemo-leadership-event.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>


    <link href="{{ asset('backends') }}/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="{{ asset('backends') }}/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="{{ asset('backends') }}/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="{{ asset('backends') }}/lib/rickshaw/rickshaw.min.css" rel="stylesheet">
    <link href="{{ asset('backends') }}/lib/highlightjs/github.css" rel="stylesheet">
    <link href="{{ asset('backends') }}/lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="{{ asset('backends') }}/lib/select2/css/select2.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('backends') }}/lib/toastr/toastr.css">
    <link href="{{ asset('backends') }}/lib/medium-editor/medium-editor.css" rel="stylesheet">
    <link href="{{ asset('backends') }}/lib/medium-editor/default.css" rel="stylesheet">
    <link href="{{ asset('backends') }}/lib/summernote/summernote-bs4.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('backends') }}/lib/bootstrap-tagsinput.css" crossorigin="anonymous">
    <link href="{{ asset('backends') }}/lib/spectrum/spectrum.css" rel="stylesheet">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <!-- Starlight CSS -->
    <link rel="stylesheet" href="{{ asset('backends') }}/css/starlight.css">
    <!--

TemplateMo 575 Leadership Event

https://templatemo.com/tm-575-leadership-event

-->

    <style>
        .img-effect {
            border-radius: 5px;
            border: 3px solid #742e0b;
            padding: 3px;
            box-shadow: #4d4d4d;
        }

        h2 {
            color: #852e03;
            font-size: 35px;
            text-decoration: underline;
            font-weight: bold;

        }
    </style>

</head>

<body>

    <nav class="navbar navbar-expand-lg" style="background-color: #C46A3D">
        <div class="container">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <a href="{{ url('/') }}" class="navbar-brand mx-auto mx-lg-0">
                <img src="{{ asset('frontend/logo/brandLogo.png') }}" alt="brand-logo" width="100px" height="auto"
                    class="rounded-circle" style="border:.2px solid black;">
                <span class="brand-text" style="font-size: 33px; color:white; margin-top:20px;">MTRS Ltd</span>
            </a>

            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a class="nav-link click-scroll" href="#section_a" style="font-size: 18px">Home</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link click-scroll" href="#section_b" style="font-size: 18px">About Us</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link click-scroll" href="#section_c" style="font-size: 18px">Products</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link click-scroll" href="#section_d" style="font-size: 18px">Photo Gallary</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link click-scroll" href="#section_e" style="font-size: 18px">Managements</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link click-scroll" href="#section_f" style="font-size: 18px">Projects</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link click-scroll" href="#section_g" style="font-size: 18px">Contact</a>
                    </li>
                </ul>
                <div>

                </div>
    </nav>

    <main class="mt-4">
        <section class="" id="section_a">
            <div class="container">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                 
                    <div class="carousel-inner">
                        @foreach ($sliders as $key => $slider)
                            <div class="carousel-item {{ $key == 1 ? 'active' : '' }}">
                                <img src="{{ asset("$slider->image") }}" class="d-block w-100" alt="...">
                            </div>
                        @endforeach

                    </div>
                    <button class="carousel-control-prev" type="button" data-target="#carouselExampleIndicators"
                        data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-target="#carouselExampleIndicators"
                        data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </button>
                </div>
            </div>

        </section>


        <section class="about" id="section_b" style="margin-top:30px;">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12 col-12 text-center mt-3 mb-3">
                        <h2 class="mb-3">About Us </h2>
                    </div>

                    <div class="col-lg-9 col-12">
                        <h3 class="mb-2">{{ $about->title }}</h3>

                        <div style=" width: 90%; text-align: justify; text-justify: inter-word;">
                            <p> {{ $about->description }}</p>
                        </div>
                    </div>

                    <div class="col-lg-3 col-12 mt-1 mt-lg-0">
                        <div class="text-center">
                            <img src="{{ asset($about->profile) }}" style="width:200px; height:270px"
                                class="img-effect" />
                            <div class="mt-2">
                                <span class="text-dark fs-4 fw-bold"> {{ $about->name }} </span>
                                <p style="font-size:16px"> {{ $about->designation }} </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>


        <section class="speakers" id="section_c">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12 col-12 d-flex flex-column justify-content-center align-items-center">
                        <div class="speakers-text-info">
                            <h2 class="mb-2 text-center">Our Products </h2>
                        </div>
                    </div>


                    <div class="col-lg-12 col-12">
                        <div class="row">
                            @foreach ($products as $product)
                                <div class="col-lg-3 col-md-6 col-12">
                                    <div class="speakers-thumb speakers-thumb-small">
                                        <img src="{{ asset($product->product_image) }}" alt=""
                                            width="250px" height="300px">
                                    </div>
                                    <h5 class="text-center mt-3" style="color:rgb(4, 4, 134)">
                                        {{ $product->product_name }}</h5>
                                </div>
                            @endforeach

                        </div>
                    </div>

                </div>
            </div>
        </section>


        <section class="schedule mt-3" id="section_d" style="height:400px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-12 text-center mb-5" style="margin-top: 50px;">
                        <h2> Our Photo Gallary </h2>
                    </div>

                    <div class="col-lg-12 col-12">
                        <!-- Modal gallery -->
                        <section class="">
                            <!-- Section: Images -->
                            <section class="">
                                <div class="row">
                                    @foreach ($gallaries as $gallary)
                                        <div class="col-lg-3 col-md-12 mb-4 mb-lg-0">
                                            <div class="bg-image hover-overlay ripple shadow-1-strong rounded"
                                                data-ripple-color="light">
                                                <img src="{{ $gallary->gallary_image }}" class="w-100" />
                                                <a href="#!" data-mdb-toggle="modal"
                                                    data-mdb-target="#exampleModal1">
                                                    <div class="mask"
                                                        style="background-color: rgba(251, 251, 251, 0.2);"></div>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </section>
                            <!-- Section: Images -->
                        </section>
                        <!-- Modal gallery -->
                    </div>

                </div>
            </div>
        </section>

        <section class="speakers" id="section_e" style="padding:25px 0">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12 col-12 d-flex flex-column justify-content-center align-items-center">
                        <div class="speakers-text-info">
                            <h2 class="mb-2 text-center"> Our Management Team </h2>
                        </div>
                    </div>


                    <div class="col-lg-12 col-12">
                        <div class="row">
                            @foreach ($managements as $management)
                                <div class="col-lg-3 col-md-6 col-12">
                                    <div class="speakers-thumb speakers-thumb-small">
                                        <img src="{{ asset($management->image) }}" alt="" width="250px"
                                            height="300px"
                                            style="border-radius: 15px 0 15px 0; padding:5px; border:1px solid grey">
                                    </div>
                                    <h5 class="text-center mt-3" style="color:rgb(4, 4, 134)">{{ $management->name }}
                                    </h5>
                                    <h6 class="text-center mt-3" style="color:rgb(7, 7, 27)">
                                        {{ $management->designation }}</h6>

                                </div>
                            @endforeach

                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="speakers" id="section_f" style="background-color:hsl(213, 51%, 86%); padding-bottom:25px">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12 col-12 d-flex flex-column justify-content-center align-items-center">
                        <div class="speakers-text-info">
                            <h2 class="mb-2 text-center">Our Projects </h2>
                        </div>
                    </div>


                    <div class="col-lg-12 col-12">
                        <div class="row">
                            <h4 class="text-center"> MTRS Lubricants Limited </h4>
                      
                            @foreach ($mtrsLubricants as $mtrsLubricant)
                                    <div class="col-lg-3 col-md-6 col-12">
                                        <div class="speakers-thumb speakers-thumb-small">
                                            <img src="{{ asset($mtrsLubricant->image) }}" alt=""
                                                width="250px" height="300px"
                                                style="border-radius: 15px 0 15px 0; padding:5px; border:1px solid grey">
                                        </div>
                                        <h5 class="text-center mt-3" style="color:rgb(4, 4, 134)">
                                            {{ $mtrsLubricant->product_name }}</h5>
                                    </div>
                            @endforeach
                       

                        </div>
                    </div>

                    <div class="col-lg-12 col-12">
                        <div class="row">
                            <h4 class="text-center"> MTRS Multimedi Limited </h4>
                            <img src="{{ asset('frontend/images/com.jpg') }}" alt=""
                                style="height: 80px; width: 160px;" class="m-auto">
                        </div>
                    </div>

                    <div class="col-lg-12 col-12">
                        <div class="row">
                            <h4 class="text-center"> MTRS Cable and Electronics Limited </h4>
                            <img src="{{ asset('frontend/images/com.jpg') }}" alt=""
                                style="height: 80px; width: 160px;" class="m-auto">
                        </div>
                    </div>

                    <div class="col-lg-12 col-12">
                        <div class="row">
                            <h4 class="text-center"> MTRS Couriers and Transportation Limited </h4>
                            <img src="{{ asset('frontend/images/com.jpg') }}" alt=""
                                style="height: 80px; width: 160px;" class="m-auto">
                        </div>
                    </div>

                    <div class="col-lg-12 col-12">
                        <div class="row">
                            <h4 class="text-center"> MTRS Couriers and Transportation Limited </h4>
                            <img src="{{ asset('frontend/images/com.jpg') }}" alt=""
                                style="height: 80px; width: 160px;" class="m-auto">
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="venue section-padding" id="section_g">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-12">
                        {{-- <iframe class="google-map"
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1511.091461689997!2d-73.9866630916883!3d40.758001294831736!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25855a96da09d%3A0x860bf5a5e1a00a68!2sTimes%20Square%2C%20New%20York%2C%20NY%2010036%2C%20USA!5e0!3m2!1sen!2ssg!4v1643035529098!5m2!1sen!2ssg"
                            width="100%" height="371.59" allowfullscreen="" loading="lazy"></iframe> --}}

                            <div class="mapouter"><div class="gmap_canvas"><iframe class="gmap_iframe" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=600&amp;height=400&amp;hl=en&amp;q=Mirpur 6, Dhaka-1216&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe><a href="https://embedmapgenerator.com/">embed google maps in website</a></div><style>.mapouter{position:relative;text-align:right;width:600px;height:400px;}.gmap_canvas {overflow:hidden;background:none!important;width:550px;height:400px;}.gmap_iframe {width:600px!important;height:400px!important; border-radius:10px;}</style></div>
                    </div>
                    <div class="col-lg-6 col-12 mt-5 mt-lg-0">
                        <div class="venue-thumb bg-white shadow-lg">

                            <div class="venue-info-title text-center">
                                <h2 class="text-white mb-0">Contact Us</h2>
                            </div>

                            <div class="venue-info-body">
                                <h4 class="d-flex">
                                    <span class="brand-text" style="margin-bottom:15px">MTRS Lubricants Ltd</span>
                                </h4>
                                <h5 class="d-flex">
                                    <i class="bi-geo-alt me-1" style="color:#1777ed"></i>
                                    <span style="color:rgb(4, 4, 134); font-size:17px;">{{ $contact->address }}</span>
                                </h5>

                                <h5 class="mt-2 mb-1">
                                    <a href="#">
                                        <i class="bi-envelope me-1"></i>
                                        <span
                                            style="color:rgb(4, 4, 134); font-size:17px;">{{ $contact->email }}</span>
                                    </a>
                                </h5>

                                <h5 class="mt-2 mb-1">
                                    <a href="#">
                                        <i class="bi-telephone me-1"></i>
                                        <span
                                            style="color:rgb(4, 4, 134); font-size:17px;">{{ $contact->phone }}</span>
                                    </a>
                                </h5>
                                <h5 class="mt-2 mb-1">
                                    <a href="#">
                                        <i class="bi-whatsapp me-1"></i>
                                        <span style="color:rgb(4, 4, 134); font-size:17px;">
                                            {{ $contact->whatsapp }}</span>
                                    </a>
                                </h5>
                                <h5 class="mt-2 mb-1">
                                    <a href="#">
                                        <i class="bi-skype me-1"></i>
                                        <span style="color:rgb(4, 4, 134); font-size:17px;"> {{ $contact->skype }}
                                        </span>
                                    </a>
                                </h5>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="contact mt-5" id="section" style="padding:20px 0">
            <div class="container">
                <div class="row">

                    <div class="col-lg-8 col-12 mx-auto">
                        <form class="custom-form contact-form bg-white shadow-lg"
                            action="{{ route('client-store') }}" method="post" role="form">
                            @csrf
                            <h2 class="text-center text-light"> Ask Question ?</h2>

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-12">
                                    <input type="text" name="name" id="name" class="form-control"
                                        placeholder="Name" required="">
                                </div>

                                <div class="col-lg-12 col-md-12 col-12">
                                    <input type="text" name="phone" id="phone" class="form-control"
                                        placeholder="phone" required="">
                                </div>

                                <div class="col-lg-12 col-md-12 col-12">
                                    <input type="email" name="email" id="email" pattern="[^ @]*@[^ @]*"
                                        class="form-control" placeholder="Email" required="">
                                </div>

                                <div class="col-lg-12 col-md-12 col-12">
                                    <input type="text" name="subject" id="subject" class="form-control"
                                        placeholder="Subject">
                                </div>

                                <div class="col-12">
                                    <textarea class="form-control" rows="5" id="message" name="message" placeholder="Message"></textarea>
                                </div>

                                <div class="text-right">
                                    <input type="submit" value="Submit" class="btn btn-primary" />
                                </div>

                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </section>

    </main>

    <footer class="mt-3" style="background-color:hsl(20, 65%, 50%); padding:30px 0">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-lg-4 col-12">
                    <a href="{{ url('/') }}" class="navbar-brand">
                        <img src="{{ asset('frontend/logo/brandLogo.png') }}" alt="brand-logo" width="80px"
                            height="auto" class="rounded-circle" style="border:.2px solid black;">
                        <span class="brand-text" style="color:white; font-size:30px; margin-top:20px">MTRS Ltd</span>
                    </a>
                </div>

                <div class="col-lg-4 col-12 ms-lg-auto">
                    <h5 class="text-center text-light mb-3" style="font-size:30px">Follow Us</h5>
                    <div class="d-flex">
                        <ul class="social-icon ms-auto text-center">
                            <li style="margin-right:35px"><a href="{{ $social->facebook }}" class="social-icon-link bi-facebook"></a>
                            </li>

                            <li style="margin-right:35px"><a href="{{ $social->instagram }}"
                                    class="social-icon-link bi-instagram"></a></li>

                            <li style="margin-right:35px"><a href="{{ $social->twitter }}" class="social-icon-link bi-twitter"></a>
                            </li>

                            <li style="margin-right:35px"><a href="{{ $social->youtube }}" class="social-icon-link bi-youtube"></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4 col-12 text-left" style="padding-left:150px">
                    <h3 class="text-light" style="font-size:30px;">Contact Us</h3>
                    <div class="text-left">
                        <span class="text-light"> {{ $contact->address }}</span>
                    </div>
                    <div class="text-left">
                        <span class="text-light"> {{ $contact->phone }}</span>
                    </div>
                    <div class="text-left">
                        <span class="text-light"> {{ $contact->email }}</span>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div class="footer-bottom" style="background-color:#4d4d4d; padding:10px 0">
        <div class="row">
            <div class="container">
                <div class="col-lg-12 col-12 text-right">
                    <span class="brand-text text-light"> Design & Developed By <a href="http://linktechbd.com">Link-Up
                            Technology Ltd</a> </span>
                </div>
            </div>
        </div>
    </div>


    <!-- google maps -->

    <script> 
        function showMap(lat, long){
            var coord = {lat:lat, lng:long};

            new google.maps.Map(
                document.getElementById("map"),
                 {
                zoom: 10,
                center: coord
            });
        } 
        
        showMap(0,0)
    </script>



</body>

</html>
