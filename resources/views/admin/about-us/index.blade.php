@extends('layouts.admin-master')
@section('about-us')
    active
@endsection
@section('admin-content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="index.html">MTRS Ltd.</a>
            <span class="breadcrumb-item active">About-Us</span>
        </nav>

        <div class="sl-pagebody">
            <div class="row row-sm">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">About us List</div>
                        <div class="card-body">
                            <div class="table table-wrapper">
                                <table id="datatable1" class="table display responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th class="wd-10p">Profile</th>
                                            <th class="wd-15p">Title</th>
                                            <th class="wd-15p">Name</th>
                                            <th class="wd-15p">Designation</th>
                                            <th class="wd-10p">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($abouts as $item)
                                            <tr>
                                                <td class="wd-15p">
                                                    <img src="{{ asset($item->profile) }}" alt=""
                                                    style="width: 80px;">

                                                </td>

                                                <td class="wd-15p">
                                                    {{ $item->title }}
                                                </td>
                            
                                                <td class="wd-20p">
                                                   {{ $item->name }}
                                                </td>
                                                <td class="wd-15p">
                                                   {{ $item->designation }}
                                                </td>
                                                <td class="wd-15p">
                                                    <a href="{{ url('admin/about-edit/' . $item->id) }}"
                                                        class="btn btn-sm btn-primary" title="edit data"> <i
                                                            class="fa fa-pencil"></i></a>

                                                    <a href="{{ url('admin/about-delete/' . $item->id) }}"
                                                        class="btn btn-sm btn-danger" id="delete" title="delete data"><i
                                                            class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div><!-- table-wrapper -->
                        </div>
                    </div><!-- card -->
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-header">Add New About Details</div>
                        <div class="card-body">
                            <form action="{{ route('about-store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label class="form-control-label">About title <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="title" value="{{ old('title') }}"
                                        placeholder="Enter about title">

                                        @error('title')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">about Description: <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="description"
                                        value="{{ old('description') }}" placeholder="Enter about description">
                                    @error('description')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Profile Image: <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="file" name="profile"
                                        placeholder="Enter brand_name_bn">
                                    @error('profile')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Profile Name: <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="name"
                                        value="{{ old('description') }}" placeholder="Enter Profile Name">
                                    @error('name')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">Designation: <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="designation"
                                        value="{{ old('description') }}" placeholder="Enter Designation">
                                    @error('designation')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-layout-footer">
                                    <button type="submit" class="btn btn-info">Add New</button>
                                </div><!-- form-layout-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
