@extends('layouts.admin-master')
@section('about-us')
    active
@endsection
@section('admin-content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="index.html">MTRS Ltd.</a>
            <span class="breadcrumb-item active">About-Us</span>
        </nav>

        <div class="sl-pagebody">
            <div class="row row-sm">
                <div class="col-md-8 m-auto">
                    <div class="card">
                        <div class="card-header"> Update About Details</div>
                        <div class="card-body">
                            <form action="{{ route('about-update') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{ $about->id }}">
                                <input type="hidden" name="old_image" value="{{ $about->profile }}">
                                <div class="form-group">
                                    <label class="form-control-label">About title <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="title" value="{{ $about->title }}"
                                        placeholder="Enter about title">

                                    @error('title')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">about Description: <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="description"
                                        value="{{ $about->description }}" placeholder="Enter about description">
                                    @error('description')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Profile Image: <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="file" name="profile"
                                        placeholder="Enter brand_name_bn">
                                    @error('profile')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <img src="{{ asset($about->profile) }}" alt="profile" width="150px" height="auto">
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Profile Name: <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="name"
                                        value="{{ $about->name }}" placeholder="Enter Profile Name">
                                    @error('name')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">Designation: <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="designation"
                                        value="{{ $about->designation  }}" placeholder="Enter Designation">
                                    @error('designation')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-layout-footer">
                                    <button type="submit" class="btn btn-info">Update</button>
                                </div><!-- form-layout-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
