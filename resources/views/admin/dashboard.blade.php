@extends('layouts.admin-master')
@section('dashboard')
    active
@endsection
@section('admin-content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="index.html">MTRS Ltd</a>
            <span class="breadcrumb-item active">Dashboard</span>
        </nav>

        <div class="sl-pagebody">
            <div class="row row-sm">
                <div class="col-md-8 m-auto">
                    <div class="card">
                        <div class="card-body">
                           <img src="{{ asset('frontend/images/dashboard.png') }}" alt="" class="m-auto">
                        </div>
                    </div><!-- card -->
                </div>
               
            </div>
        </div>


    </div>
@endsection
