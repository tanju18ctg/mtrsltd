@extends('layouts.admin-master')
@section('sliders')
    active
@endsection
@section('admin-content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="index.html">MTRS Ltd.</a>
            <span class="breadcrumb-item active">Slider</span>
        </nav>

        <div class="sl-pagebody">
            <div class="row row-sm">
                <div class="col-md-8 m-auto">
                    <div class="card">
                        <div class="card-header">Update Slider</div>
                        <div class="card-body">
                            <form action="{{ route('slider-update') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{ $slider->id }}">
                                <input type="hidden" name="old_image" value="{{ $slider->image }}">
                                <div class="form-group">
                                    <label class="form-control-label">Slider Title English: <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="title" value="{{ $slider->title }}"
                                        placeholder="Enter slider title English">
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">SLider Description: <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="description"
                                        value="{{ $slider->description }}" placeholder="Enter description">
                                    @error('description')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Slider Image: <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="file" name="image"
                                        placeholder="Enter brand_name_bn">
                                    @error('image')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <img src="{{ asset( $slider->image ) }}" alt="" width="200px" height="auto">
                                </div>
                                <div class="form-layout-footer">
                                    <button type="submit" class="btn btn-info">Update</button>
                                </div><!-- form-layout-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
