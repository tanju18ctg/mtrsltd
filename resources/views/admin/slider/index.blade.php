@extends('layouts.admin-master')
@section('sliders')
    active
@endsection
@section('admin-content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="index.html">SHopMama</a>
            <span class="breadcrumb-item active">Slider</span>
        </nav>

        <div class="sl-pagebody">
            <div class="row row-sm">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">sliders List</div>
                        <div class="card-body">
                            <div class="table table-wrapper">
                                <table id="datatable1" class="table display responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th class="wd-30p">Slider Image</th>
                                            <th class="wd-25p">Slider title </th>
                                            <th class="wd-25p">description</th>
                                            <th class="wd-20p">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sliders as $item)
                                            <tr>
                                                <td>
                                                    <img src="{{ asset($item->image) }}" alt=""
                                                        style="width: 80px;">
                                                </td>
                                                <td>
                                                    @if ($item->title == null)
                                                        <span class="badge badg-pill badge-danger">No Title Found</span>
                                                    @else
                                                        {{ $item->title}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($item->description == null)
                                                        <span class="badge badg-pill badge-danger">No Descp Found</span>
                                                    @else
                                                        {{ $item->description }}
                                                    @endif

                                                </td>

                                                <td>
                                                    <a href="{{ url('admin/slider-edit/' . $item->id) }}"
                                                        class="btn btn-sm btn-primary" title="edit data"> <i
                                                            class="fa fa-pencil"></i></a>

                                                    <a href="{{ url('admin/slider-delete/' . $item->id) }}"
                                                        class="btn btn-sm btn-danger" id="delete" title="delete data"><i
                                                            class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div><!-- table-wrapper -->
                        </div>
                    </div><!-- card -->
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">Add New Slider</div>
                        <div class="card-body">
                            <form action="{{ route('slider-store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label class="form-control-label">Slider Title English: <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="title" value="{{ old('title') }}"
                                        placeholder="Enter slider title English">
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">SLider Description: <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="description"
                                        value="{{ old('description') }}" placeholder="Enter description">
                                    @error('description')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Slider Image: <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="file" name="image"
                                        placeholder="Enter brand_name_bn">
                                    @error('image')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-layout-footer">
                                    <button type="submit" class="btn btn-info">Add New</button>
                                </div><!-- form-layout-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
