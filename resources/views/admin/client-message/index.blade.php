@extends('layouts.admin-master')
@section('client-message')
    active
@endsection
@section('admin-content')
     <!-- ########## START: MAIN PANEL ########## -->
     <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
          <a class="breadcrumb-item" href="index.html">MTRL Ltd</a>
          <span class="breadcrumb-item active">Dashboard</span>
        </nav>

        <div class="sl-pagebody">
          <div class="row row-sm">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">Client Message List</div>
                <div class="card-body">
                <div class="table-wrapper">
                  <table id="datatable1" class="table display responsive nowrap">
                    <thead>
                      <tr>
                        <th class="wd-15p text-center">Name</th>
                        <th class="wd-15p text-center">Phone</th>
                        <th class="wd-15p text-center">Email</th>
                        <th class="wd-15p text-center">Subject</th>
                        <th class="wd-30p text-center">Message</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($messages as $item)
                      <tr>
                        <td class="text-center">
                            {{ $item->name}}
                        </td>
                        <td class="text-center">{{ $item->phone }}</td>
                        <td class="text-center">{{ $item->email }}</td>
                        <td class="text-center">{{ $item->subject }}</td>
                        <td class="text-center">{{ $item->message }}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div><!-- table-wrapper -->
              </div>
              </div><!-- card -->
            </div>
            
          </div>
        </div>


    </div>
@endsection
