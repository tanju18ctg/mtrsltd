  <!-- ########## START: LEFT PANEL ########## -->
    <div class="sl-logo"><a href="#"><i class="icon ion-android-star-outline"></i> MTRS Lubricants</a></div>
    <div class="sl-sideleft">
      <div class="sl-sideleft-menu">

        <a href="{{ url ('/') }}" class="sl-menu-link" target="_blank">
          <div class="sl-menu-item">
            <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
            <span class="menu-item-label">Visit Site</span>
          </div><!-- menu-item -->
        </a><!-- sl-menu-link -->

        <a href="{{ route('dashboard') }}" class="sl-menu-link @yield('dashboard')">
            <div class="sl-menu-item">
              <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
              <span class="menu-item-label">Dashboard</span>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->

          <a href="{{ route('sliders') }}" class="sl-menu-link @yield('sliders')">
            <div class="sl-menu-item">
              <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
              <span class="menu-item-label">Sliders</span>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
    

       
          <a href="{{ route('products') }}" class="sl-menu-link @yield('products')">
            <div class="sl-menu-item">
              <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
              <span class="menu-item-label">Products</span>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->

          <a href="{{ route('gallary') }}" class="sl-menu-link @yield('gallary')">
            <div class="sl-menu-item">
              <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
              <span class="menu-item-label">Photo Gallary</span>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->

          <a href="{{ route('about-us') }}" class="sl-menu-link @yield('about-us')">
            <div class="sl-menu-item">
              <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
              <span class="menu-item-label">About Us</span>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->

          <a href="{{ route('management') }}" class="sl-menu-link @yield('management')">
            <div class="sl-menu-item">
              <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
              <span class="menu-item-label">Management</span>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->

          <a href="{{ route('contact') }}" class="sl-menu-link @yield('contact')">
            <div class="sl-menu-item">
              <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
              <span class="menu-item-label">Add Contact</span>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <a href="{{ route('social-link') }}" class="sl-menu-link @yield('social-link')">
            <div class="sl-menu-item">
              <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
              <span class="menu-item-label">Add Social Link</span>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->

          <a href="{{ route('client-message') }}" class="sl-menu-link @yield('client-message')">
            <div class="sl-menu-item">
              <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
              <span class="menu-item-label">Client Messages</span>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->


          <a href="#" class="sl-menu-link @yield('projects')">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Projects</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->

          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="{{ route('mtrs-lubricants') }}" class="nav-link @yield('mtrs-lubricants')"> MTRS Lubricant Ltd </a></li>
            <li class="nav-item"><a href="{{ route('mtrs-multimedia') }}" class="nav-link @yield('mtrs-multimedia')">MTRS Multimedia Ltd</a></li>
            <li class="nav-item"><a href="{{ route('mtrs-cable') }}" class="nav-link @yield('mtrs-cable')">MTRS Cable & Elect Ltd</a></li>
            <li class="nav-item"><a href="{{ route('mtrs-courier') }}" class="nav-link @yield('mtrs-courier')">MTRS Couriers Ltd</a></li>
            <li class="nav-item"><a href="{{ route('mtrs-food') }}" class="nav-link @yield('mtrs-food')">MTRS Food & Beverage Ltd</a></li>
          </ul>

      </div><!-- sl-sideleft-menu -->
      <br>
    </div><!-- sl-sideleft -->
    <!-- ########## END: LEFT PANEL ########## -->