@extends('layouts.admin-master')

@section('projects') active show-sub @endsection
@section('mtrs-lubricants') active @endsection

@section('admin-content')
     <!-- ########## START: MAIN PANEL ########## -->
     <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
          <a class="breadcrumb-item" href="index.html">MTRS Ltd.</a>
          <span class="breadcrumb-item active">Dashboard</span>
        </nav>

        <div class="sl-pagebody">
          <div class="row row-sm">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header">Product List</div>
                <div class="card-body">
                <div class="table-wrapper">
                  <table id="datatable1" class="table display responsive nowrap">
                    <thead>
                      <tr>
                        <th class="wd-30p">Product Image</th>
                        <th class="wd-40p">Product name </th>
                        <th class="wd-30p">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($mtrsLubricants as $item)
                      <tr>
                        <td>
                          <img src="{{ asset($item->image) }}" alt="" style="width: 80px;">
                        </td>
                        <td>{{ $item->product_name }}</td>
                        <td>
                          <a href="{{ url('admin/mtrs-lubricants-edit/'.$item->id) }}" class="btn btn-sm btn-primary" title="edit data"> <i class="fa fa-pencil"></i></a>

                          <a href="{{ url('admin/mtrs-lubricants-delete/'.$item->id) }}" class="btn btn-sm btn-danger" id="delete" title="delete data"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div><!-- table-wrapper -->
              </div>
              </div><!-- card -->
            </div>
            <div class="col-md-4">
              <div class="card">
                <div class="card-header">Add New Employee</div>
                  <div class="card-body">
                <form action="{{ route('mtrs-lubricants-store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="form-group">
                      <label class="form-control-label">Product Name : <span class="tx-danger">*</span></label>
                      <input class="form-control" type="text" name="product_name" value="{{ old('product_name') }}" placeholder="Enter product name">
                      @error('product_name')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>


                    <div class="form-group">
                      <label class="form-control-label">Product Photo: <span class="tx-danger">*</span></label>
                      <input class="form-control" type="file" name="image">
                      @error('image')
                      <span class="text-danger">{{ $message }}</span>
                   @enderror
                    </div>
                    <div class="form-layout-footer">
                      <button type="submit" class="btn btn-info">Add New</button>
                    </div><!-- form-layout-footer -->
                  </form>
                  </div>
              </div>
            </div>
          </div>
        </div>


    </div>
@endsection
