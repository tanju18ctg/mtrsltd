@extends('layouts.admin-master')

@section('projects')
    active show-sub
@endsection
@section('mtrs-lubricants')
    active
@endsection

@section('admin-content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="index.html">SHopMama</a>
            <span class="breadcrumb-item active">Dashboard</span>
        </nav>

        <div class="sl-pagebody">
            <div class="row row-sm">
                <div class="col-md-8 m-auto">
                    <div class="card">
                        <div class="card-header">Update MTRS Project</div>
                        <div class="card-body">
                            <form action="{{ route('mtrs-food-update') }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{ $mtrsFood->id }}">
                                <input type="hidden" name="old_image" value="{{ $mtrsFood->image }}">
                                <div class="form-group">
                                    <label class="form-control-label">Product Name : <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="product_name"
                                        value="{{ $mtrsFood->product_name }}" placeholder="Enter product name">
                                    @error('product_name')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>


                                <div class="form-group">
                                    <label class="form-control-label">Product Photo: <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="file" name="image">
                                    @error('image')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <img src="{{ asset($mtrsFood->image) }}" alt=""
                                        style="height:200px; width:150px">
                                </div>

                                <div class="form-layout-footer">
                                    <button type="submit" class="btn btn-info">Update</button>
                                </div><!-- form-layout-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
