@extends('layouts.admin-master')
@section('products')
    active
@endsection
@section('admin-content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="index.html">MTRS Ltd.</a>
            <span class="breadcrumb-item active">Dashboard</span>
        </nav>

        <div class="sl-pagebody">
            <div class="row row-sm">
                <div class="col-md-8 m-auto">
                    <div class="card">
                        <div class="card-header">Update Product</div>
                        <div class="card-body">
                            <form action="{{ route('update-product') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="old_image" value="{{ $product->product_image }}">
                                <input type="hidden" name="product_id" value="{{ $product->id }}">
                                <div class="form-group">
                                    <label class="form-control-label">Product Name : <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="product_name"
                                        value="{{ $product->product_name }}" placeholder="Enter product name">
                                    @error('product_name')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>


                                <div class="form-group">
                                    <label class="form-control-label">Product Image: <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="file" name="product_image">
                                    @error('product_image')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <img src="{{ asset( $product->product_image) }}" alt="">
                            </div>
                            <div class="form-layout-footer">
                                <button type="submit" class="btn btn-info">Update</button>
                            </div><!-- form-layout-footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection
