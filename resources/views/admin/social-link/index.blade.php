@extends('layouts.admin-master')
@section('social-link')
    active
@endsection
@section('admin-content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="index.html">SHopMama</a>
            <span class="breadcrumb-item active">Dashboard</span>
        </nav>

        <div class="sl-pagebody">
            <div class="row row-sm">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Social Link List</div>
                        <div class="card-body">
                            <div class="table-wrapper">
                                <table id="datatable1" class="table display responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th class="wd-30p">Facebook Link</th>
                                            <th class="wd-25p">Youtube Link </th>
                                            <th class="wd-25p">Twitter Link</th>
                                            <th class="wd-25p">Instagram Link</th>
                                            <th class="wd-20p">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($socials as $item)
                                            <tr>
                                                <td>{{ $item->facebook }}</td>
                                                <td>{{ $item->youtube }}</td>
                                                <td>{{ $item->twitter }}</td>
                                                <td>{{ $item->instagram }}</td>
                                                <td>
                                                    <a href="{{ url('social-link-edit/' . $item->id) }}"
                                                        class="btn btn-sm btn-primary" title="edit data"> <i
                                                            class="fa fa-pencil"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div><!-- table-wrapper -->
                        </div>
                    </div><!-- card -->
                </div>
                {{-- <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">Add New Brand</div>
                        <div class="card-body">
                            <form action="{{ route('social-link-store') }}" method="POST">
                                @csrf

                                <div class="form-group">
                                    <label class="form-control-label">Facebook Link : <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="facebook"
                                        value="{{ old('facebook') }}" placeholder="Enter Facebook Link">
                                    @error('facebook')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>


                                <div class="form-group">
                                    <label class="form-control-label">Youtube Link : <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="youtube"
                                        value="{{ old('youtube') }}" placeholder="Enter Youtube Link">
                                    @error('youtube')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>


                                <div class="form-group">
                                    <label class="form-control-label">Twitter Link: <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="twitter" placeholder="Enter twitter Link">
                                    @error('twitter')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Instagram Link: <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="instagram" placeholder="Enter instagram Link">
                                    @error('instagram')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-layout-footer">
                                    <button type="submit" class="btn btn-info">Add New</button>
                                </div><!-- form-layout-footer -->
                            </form>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>


    </div>
@endsection
