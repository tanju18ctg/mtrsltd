@extends('layouts.admin-master')
@section('social-link')
    active
@endsection
@section('admin-content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="index.html">SHopMama</a>
            <span class="breadcrumb-item active">Dashboard</span>
        </nav>

        <div class="sl-pagebody">
            <div class="row row-sm">
                <div class="col-md-8 m-auto">
                    <div class="card">
                        <div class="card-header">Update Social Link</div>
                        <div class="card-body">
                            <form action="{{ route('social-link-update') }}" method="POST">
                                @csrf

                                <input type="hidden" name="id" value="{{ $social->id }}">
                                <div class="form-group">
                                    <label class="form-control-label">Facebook Link : <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="facebook" 
                                        placeholder="Enter Facebook Link" value="{{ $social->facebook }}">
                                    @error('facebook')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>


                                <div class="form-group">
                                    <label class="form-control-label">Youtube Link : <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="youtube" value="{{ $social->youtube }}"
                                        placeholder="Enter Youtube Link" >
                                    @error('youtube')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>


                                <div class="form-group">
                                    <label class="form-control-label">Twitter Link: <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="twitter"
                                        placeholder="Enter twitter Link" value="{{ $social->twitter }}">
                                    @error('twitter')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Instagram Link: <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="instagram"
                                        placeholder="Enter instagram Link" value="{{ $social->instagram }}">
                                    @error('instagram')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-layout-footer">
                                    <button type="submit" class="btn btn-info">Update</button>
                                </div><!-- form-layout-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
