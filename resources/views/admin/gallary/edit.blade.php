@extends('layouts.admin-master')
@section('gallary')
    active
@endsection
@section('admin-content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="index.html">MTRS Ltd.</a>
            <span class="breadcrumb-item active">Dashboard</span>
        </nav>

        <div class="sl-pagebody">
            <div class="row row-sm">
                <div class="col-md-8 m-auto">
                    <div class="card">
                        <div class="card-header">Update Gallary</div>
                        <div class="card-body">
                            <form action="{{ route('gallary-update') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{ $gallary->id }}">
                                <input type="hidden" name="old_image" value="{{ $gallary->gallary_image }}">
                                <div class="form-group">
                                    <label class="form-control-label">Title : <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="title" value="{{ $gallary->title }}"
                                        placeholder="Enter title">
                                    @error('title')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Gallary Image: <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="file" name="gallary_image">
                                    @error('product_image')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <img src="{{ asset($gallary->gallary_image) }}" alt="">
                                </div>
                                <div class="form-layout-footer">
                                    <button type="submit" class="btn btn-info">Update</button>
                                </div><!-- form-layout-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
