@extends('layouts.admin-master')
@section('contact')
    active
@endsection
@section('admin-content')
     <!-- ########## START: MAIN PANEL ########## -->
     <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
          <a class="breadcrumb-item" href="index.html">MTRL Ltd</a>
          <span class="breadcrumb-item active">Dashboard</span>
        </nav>

        <div class="sl-pagebody">
          <div class="row row-sm">
            <div class="col-md-10 m-auto">
              <div class="card">
                <div class="card-header">Contact List</div>
                <div class="card-body">
                <div class="table-wrapper">
                  <table id="datatable1" class="table display responsive nowrap">
                    <thead>
                      <tr>
                        <th class="wd-30p">Phone</th>
                        <th class="wd-25p">Address</th>
                        <th class="wd-25p">Email</th>
                        <th class="wd-25p">WhatsApp</th>
                        <th class="wd-25p">Skype</th>
                        <th class="wd-20p">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($contacts as $item)
                      <tr>
                        <td>
                            {{ $item->phone}}
                        </td>
                        <td>{{ $item->address }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->whatsapp }}</td>
                        <td>{{ $item->skype }}</td>
                        <td>
                          <a href="{{ url('admin/contact-edit/'.$item->id) }}" class="btn btn-sm btn-primary" title="edit data"> <i class="fa fa-pencil"></i></a>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div><!-- table-wrapper -->
              </div>
              </div><!-- card -->
            </div>
            {{-- <div class="col-md-4">
              <div class="card">
                <div class="card-header">Add New Contact</div>
                  <div class="card-body">
                <form action="{{ route('contact-store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="form-group">
                      <label class="form-control-label">Phone : <span class="tx-danger">*</span></label>
                      <input class="form-control" type="text" name="phone" value="{{ old('brand_name_en') }}" placeholder="Enter phone number">
                      @error('phone')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>


                    <div class="form-group">
                      <label class="form-control-label">Address : <span class="tx-danger">*</span></label>
                      <input class="form-control" type="text" name="address" value="{{ old('product name') }}" placeholder="Enter Address">
                      @error('address')
                          <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>


                    <div class="form-group">
                      <label class="form-control-label">Email: <span class="tx-danger">*</span></label>
                      <input class="form-control" type="text" name="email">
                      @error('email')
                      <span class="text-danger">{{ $message }}</span>
                   @enderror
                    </div>
                    <div class="form-layout-footer">
                      <button type="submit" class="btn btn-info">Add New</button>
                    </div><!-- form-layout-footer -->
                  </form>
                  </div>
              </div>
            </div> --}}
          </div>
        </div>


    </div>
@endsection
