@extends('layouts.admin-master')
@section('contact')
    active
@endsection
@section('admin-content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="index.html">MTRL Ltd</a>
            <span class="breadcrumb-item active">Dashboard</span>
        </nav>

        <div class="sl-pagebody">
            <div class="row row-sm">
                
                <div class="col-md-8 m-auto">
                    <div class="card">
                        <div class="card-header">Update Contact</div>
                        <div class="card-body">
                            <form action="{{ route('contact-update') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                    <input type="hidden" name="id" value="{{ $contact->id }}">
                                <div class="form-group">
                                    <label class="form-control-label">Phone : <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="phone"
                                        value="{{ $contact->phone }}" placeholder="Enter phone number">
                                    @error('phone')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>


                                <div class="form-group">
                                    <label class="form-control-label">Address : <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="address"
                                        value="{{ $contact->address }}" placeholder="Enter Address">
                                    @error('address')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>


                                <div class="form-group">
                                    <label class="form-control-label">Email: <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="email" value="{{ $contact->email }}">
                                    @error('email')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">WhatsApp: <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="whatsapp" value="{{ $contact->whatsapp }}">
                                    @error('email')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Skype: <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="skype" value="{{ $contact->skype }}">
                                    @error('email')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-layout-footer">
                                    <button type="submit" class="btn btn-info">Update</button>
                                </div><!-- form-layout-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
