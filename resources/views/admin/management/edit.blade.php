@extends('layouts.admin-master')
@section('management')
    active
@endsection
@section('admin-content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="index.html">MTRS Ltd</a>
            <span class="breadcrumb-item active">Dashboard</span>
        </nav>

        <div class="sl-pagebody">
            <div class="row row-sm">
                <div class="col-md-8 m-auto">
                    <div class="card">
                        <div class="card-header">Add New Employee</div>
                        <div class="card-body">
                            <form action="{{ route('management-update') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{ $management->id }}">
                                <input type="hidden" name="old_image" value="{{ $management->image }}">

                                <div class="form-group">
                                    <label class="form-control-label">Employee Name : <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="name" value="{{ $management->name }}"
                                        placeholder="Enter product name">
                                    @error('name')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>


                                <div class="form-group">
                                    <label class="form-control-label">Designation : <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="designation"
                                        value="{{ $management->designation }}" placeholder="Enter product description">
                                    @error('designation')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>


                                <div class="form-group">
                                    <label class="form-control-label">Empmloyee Photo: <span
                                            class="tx-danger">*</span></label>
                                    <input class="form-control" type="file" name="image">
                                    @error('image')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                   <img src="{{ asset($management->image) }}" alt="profile" style="width:200px; height:250px;">
                                </div>

                                <div class="form-layout-footer">
                                    <button type="submit" class="btn btn-info">Update</button>
                                </div><!-- form-layout-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
