<?php

namespace App\Http\Controllers\Frontend;

use Carbon\Carbon;
use App\Models\ClientReview;
use Illuminate\Http\Request;
use Facade\FlareClient\Http\Client;
use App\Http\Controllers\Controller;

class ClientReviewController extends Controller
{
    //

    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|min:4',
            'phone' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ],
        [
        'name.required' => 'Please Give name',
        'phone.required' => 'Please Give phone',
        'email.required' => 'Please upload email',
        'subject.required' => 'Please upload subject',
        'message.required' => 'Please upload message',
        ]);

        try{
            ClientReview::insert([
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'subject' => $request->subject,
                'message' => $request->message,
            ]);
    
        return redirect()->back()->with('insert', 'Data has been insert successfully');
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Data has been insert Fail..!');
        }
    }

    public function index(){
      $messages = ClientReview::latest()->get();
      return view('admin.client-message.index', compact('messages'));
    }
}
