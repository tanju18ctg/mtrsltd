<?php

namespace App\Http\Controllers\Frontend;

use App\Models\About;
use App\Models\Slider;
use App\Models\Contact;
use App\Models\Gallary;
use App\Models\Product;
use App\Models\Management;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MtrsLubricant;
use App\Models\SocialLink;

class FrontendController extends Controller
{
    //index page

    public function index()
    {
        $products = Product::latest()->get();
        $sliders = Slider::where('status', 1)->get();
        $contact = Contact::get()->first();
        $social = SocialLink::get()->first();
        $gallaries = Gallary::latest()->get();
        $about = About::latest()->get()->first();
        $managements = Management::latest()->get();
        $mtrsLubricants = MtrsLubricant::latest()->get();
        return view('frontend.index', compact('products', 'sliders', 'gallaries', 'contact', 'about', 'managements', 'mtrsLubricants', 'social'));
    }



    // public function productIndex(){
    //     return view('frontend.products');
    // }

    // public function gallaryIndex(){
    //     return view('frontend.gallary');
    // }

    // public function aboutIndex(){
    //     return view('frontend.about');
    // }

    // public function contactIndex(){
    //     return view('frontend.contact');
    // }
}
