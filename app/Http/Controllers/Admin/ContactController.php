<?php

namespace App\Http\Controllers\Admin;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class ContactController extends Controller
{
    //

    public function index(){
        $contacts = Contact::latest()->get();
        return view('admin.contact.index', compact('contacts'));
    }

    public function store(Request $request)
    {

        
        $request->validate([
            'phone' => 'required|min:4',
            'address' => 'required',
            'email' => 'required',
            'whatsapp' => 'required',
            'skype' => 'required',
        ],
        [
        'phone.required' => 'Please Give phone',
        'address.required' => 'Please Give address',
        'email.required' => 'Please upload email',
        'whatsapp.required' => 'Please Give whatsapp',
        'skype.required' => 'Please Give skype',
        ]);

        try{
            Contact::insert([
                'phone' => $request->phone,
                'address' => $request->address,
                'email' => $request->email,
                'whatsapp' => $request->whatsapp,
                'skype' => $request->skype,
            ]);
    
            return redirect()->back()->with('insert', 'Data has been insert successfully');
        } catch(\Exception $e){
            return redirect()->back()->with('error', 'Data has been insert Fail..!');
        }
     
    }

    public function edit($id){
        $contact = Contact::find($id);
        return view('admin.contact.edit', compact('contact'));
    }

    public function update(Request $request){

       $id =  $request->id;
       Contact::find($id)->update([
        'phone' => $request->phone,
        'address' => $request->address,
        'email' => $request->email,
        'whatsapp' => $request->whatsapp,
        'skype' => $request->skype,
       ]);
       return redirect()->route('contact');
    }

    public function delete($id){
        Contact::find($id)->delete();
        return redirect()->back();
    }

    
}
