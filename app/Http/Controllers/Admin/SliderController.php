<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class SliderController extends Controller
{
    //index page

    public function index()
    {
        $sliders = Slider::latest()->get();
        return view('admin.slider.index', compact('sliders'));
    }

    
    //store
    public function store (Request $request){

        $request->validate([
            'image' => 'required|min:4',
        ],
    [
        'image.required' => 'Please Give image',
    ]);

       try{
        $image = $request->file('image');
        $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(870,370)->save('uploads/slider/'.$name_gen);
        $save_url = 'uploads/slider/'.$name_gen;

        Slider::insert([
            'title' => $request->title,
            'description' => $request->description,
            'image' => $save_url,
        ]);
        return redirect()->back()->with('insert', 'Data has been insert successfully');
       }catch (\Exception $e){
        return redirect()->back()->with('error', 'Data has been insert Fail..!');
       }
    }

    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('admin.slider.edit', compact('slider'));
    }

    public function update(Request $request)
    {
        try{
            $id = $request->id;
            $old_image = $request->old_image;
            unlink(($old_image));
    
            $image = $request->file('image');
            $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(870,370)->save('uploads/slider/'.$name_gen);
            $save_url = 'uploads/slider/'.$name_gen;
    
            Slider::findOrFail($id)->update([
                'title' => $request->title,
                'description' => $request->description,
                'image' => $save_url,
            ]);
            return redirect()->route('sliders');
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Data has been update Fail..!');
        }
    }
    

    public function delete($id)
    {
        Slider::findOrFail($id)->delete();
        return redirect()->back();
    }

}
