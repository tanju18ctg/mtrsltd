<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SocialLink;
use Illuminate\Http\Request;

class SocialLinkController extends Controller
{
    //index
    public function index()
    {
        $socials = SocialLink::latest()->get();
        return view('admin.social-link.index', compact('socials'));
    }

    public function store(Request $request){
        try{
            SocialLink::insert([
                'facebook' => $request->facebook,
                'youtube' => $request->youtube,
                'twitter' => $request->twitter,
                'instagram' => $request->instagram,
            ]);
    
            return redirect()->back()->with('insert', 'Data has been insert successfully');
        }catch (\Exception $e) {
            return redirect()->back()->with('error', 'Data has been insert Fail..!');
        }
    }

    public function edit($id){
        $social = SocialLink::find($id);
        return view('admin.social-link.edit', compact('social'));
    }

    public function update(Request $request){
        $id =  $request->id;
        SocialLink::find($id)->update([
            'facebook' => $request->facebook,
            'youtube' => $request->youtube,
            'twitter' => $request->twitter,
            'instagram' => $request->instagram,
        ]);
        return redirect()->route('social-link');
    }
}
