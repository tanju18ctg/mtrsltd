<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\MtrsLubricant;
use App\Http\Controllers\Controller;
use App\Models\MtrsCable;
use App\Models\MtrsCourier;
use App\Models\MtrsFood;
use App\Models\MtrsMultimedia;
use Intervention\Image\Facades\Image;

class ProjectController extends Controller
{
    //index
    public function index(){
       $mtrsLubricants = MtrsLubricant::latest()->get();
        return view('admin.projects.mtrs-lubricants.index',compact('mtrsLubricants'));
    }

    //store
    public function store(Request $request)
    {

        $request->validate([
            'product_name' => 'required|min:4',
            'image' => 'required',
        ],
    [
        'product_name.required' => 'Please Give product name',
        'image.required' => 'Please Upload image',
    ]);
        try{
            $image = $request->file('image');
            $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(280,320)->save('uploads/mtrs-lubricants/'.$name_gen);
            $save_url = 'uploads/mtrs-lubricants/'.$name_gen;

        MtrsLubricant::insert([
            'product_name' => $request->product_name,
            'image' => $save_url,
        ]);

        return redirect()->back()->with('insert', 'Data has been insert successfully');
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Data has been insert Fail..!');
        }
    }

    public function edit($id){
        $mtrsLubricant = MtrsLubricant::find($id);
        return view('admin.projects.mtrs-lubricants.edit', compact('mtrsLubricant'));
    }

    public function update(Request $request)
    {
        try{

            $id = $request->id;
            $old_image = $request->old_image;
            
       if ($request->file('image')) {
            unlink(($old_image));
    
            $image = $request->file('image');
            $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(300,350)->save('uploads/mtrs-lubricants/'.$name_gen);
            $save_url = 'uploads/mtrs-lubricants/'.$name_gen;
    
            MtrsLubricant::find($id)->update([
                'product_name' => $request->product_name,
                'image' => $save_url,
            ]);
            return redirect()->route('mtrs-lubricants');
        }else{
            MtrsLubricant::find($id)->update([
                'product_name' => $request->product_name,
            ]);
            return redirect()->route('mtrs-lubricants');
        }
    }
        
        catch(\Exception $e){
            return redirect()->back()->with('error', 'Data has been update Fail..!');
        }
        
    }

    public function delete($id)
    {
        MtrsLubricant::find($id)->delete();
        return redirect()->back();
    }

    
    // Mutlimedia Project functions //

    public function MultimediaIndex(){
        $mtrsMultimedia = MtrsMultimedia::latest()->get();
        return view('admin.projects.mtrs-multimedia.index',compact('mtrsMultimedia'));
    }

    public function multimediaStore(Request $request){

        $request->validate([
            'product_name' => 'required|min:4',
            'image' => 'required',
        ],
    [
        'product_name.required' => 'Please Give product name',
        'image.required' => 'Please Upload image',
    ]);
        try{
            $image = $request->file('image');
            $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(280,320)->save('uploads/mtrs-multimedia/'.$name_gen);
            $save_url = 'uploads/mtrs-multimedia/'.$name_gen;
    
            MtrsMultimedia::insert([
                'product_name' => $request->product_name,
                'image' => $save_url,
            ]);
    
            return redirect()->back()->with('insert', 'Data has been insert successfully');
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Data has been insert Fail..!');
        }
       
    }

    public function multimediaEdit($id){
        $mtrsMultimedia = MtrsMultimedia::find($id);
        return view('admin.projects.mtrs-multimedia.edit', compact('mtrsMultimedia'));
    }

    public function multimediaUpdate(Request $request)
    {
             $id = $request->id;
            $old_image = $request->old_image;
            
       if ($request->file('image')) {
            unlink(($old_image));
    
            $image = $request->file('image');
            $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(300,350)->save('uploads/mtrs-multimedia/'.$name_gen);
            $save_url = 'uploads/mtrs-multimedia/'.$name_gen;
    
            MtrsMultimedia::find($id)->update([
                'product_name' => $request->product_name,
                'image' => $save_url,
            ]);
            return redirect()->route('mtrs-multimedia');
        }else{
            MtrsMultimedia::find($id)->update([
                'product_name' => $request->product_name,
            ]);
            return redirect()->route('mtrs-multimedia');
        }
    }

    public function multimediaDelete($id)
    {
        MtrsMultimedia::find($id)->delete();
        return redirect()->back();
    }

    // cable functionality

    public function cableIndex(){
        $mtrsCable = MtrsCable::latest()->get();
        return view('admin.projects.mtrs-cable.index',compact('mtrsCable'));
    }

    public function cableStore(Request $request){

        $request->validate([
            'product_name' => 'required|min:4',
            'image' => 'required',
        ],
    [
        'product_name.required' => 'Please Give product name',
        'image.required' => 'Please Upload image',
    ]);
        try{
            $image = $request->file('image');
            $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(280,320)->save('uploads/mtrs-cable/'.$name_gen);
            $save_url = 'uploads/mtrs-cable/'.$name_gen;
    
            MtrsCable::insert([
                'product_name' => $request->product_name,
                'image' => $save_url,
            ]);
    
         return redirect()->back()->with('insert', 'Data has been insert successfully');
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Data has been insert Fail..!');
        }
     
    }

    public function cableEdit($id){
        $mtrsCable = MtrsCable::find($id);
        return view('admin.projects.mtrs-cable.edit', compact('mtrsCable'));
    }

    public function cableUpdate(Request $request){
        $id = $request->id;
        $old_image = $request->old_image;
        
   if ($request->file('image')) {
        unlink(($old_image));

        $image = $request->file('image');
        $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(300,350)->save('uploads/mtrs-cable/'.$name_gen);
        $save_url = 'uploads/mtrs-cable/'.$name_gen;

        MtrsCable::find($id)->update([
            'product_name' => $request->product_name,
            'image' => $save_url,
        ]);
        return redirect()->route('mtrs-cable');
    }else{
        MtrsCable::find($id)->update([
            'product_name' => $request->product_name,
        ]);
        return redirect()->route('mtrs-cable');
    }
    }

    public function cableDelete($id)
    {
        MtrsMultimedia::find($id)->delete();
        return redirect()->back();
    }

    // Courier Functionality

    public function Courierindex()
    {
        $mtrsCourier = MtrsCourier::latest()->get();
        return view('admin.projects.mtrs-courier.index',compact('mtrsCourier'));
    }

    public function courierStore(Request $request)
    {
        $request->validate([
            'product_name' => 'required|min:4',
            'image' => 'required',
            ],
        [
            'product_name.required' => 'Please Give product name',
            'image.required' => 'Please Upload image',
        ]);
        
        try{
            $image = $request->file('image');
            $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(280,320)->save('uploads/mtrs-courier/'.$name_gen);
            $save_url = 'uploads/mtrs-courier/'.$name_gen;
    
            MtrsCourier::insert([
                'product_name' => $request->product_name,
                'image' => $save_url,
            ]);
            return redirect()->back()->with('insert', 'Data has been insert successfully');
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Data has been insert Fail..!');
        }
    

      
    }

    public function courierEdit($id){
        $mtrsCourier = MtrsCourier::find($id);
        return view('admin.projects.mtrs-courier.edit', compact('mtrsCourier'));
    }

    public function courierUpdate(Request $request)
    {
        $id = $request->id;
        $old_image = $request->old_image;
        
        if ($request->file('image')) {
                unlink(($old_image));

                $image = $request->file('image');
                $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
                Image::make($image)->resize(300,350)->save('uploads/mtrs-courier/'.$name_gen);
                $save_url = 'uploads/mtrs-courier/'.$name_gen;

                MtrsCourier::find($id)->update([
                    'product_name' => $request->product_name,
                    'image' => $save_url,
                ]);
                return redirect()->route('mtrs-courier');
            }else{
                MtrsCourier::find($id)->update([
                    'product_name' => $request->product_name,
                ]);
                return redirect()->route('mtrs-courier');
            }

    }

    public function courierDelete($id){
        MtrsCourier::find($id)->delete();
        return redirect()->back();
    }

    // mtrs-food fucntionality

    public function foodIndex()
    {
        $mtrsFood = MtrsFood::latest()->get();
        return view('admin.projects.mtrs-food.index',compact('mtrsFood'));
    }

    public function foodStore(Request $request)
    {

        $request->validate([
            'product_name' => 'required|min:4',
            'image' => 'required',
        ],
        [
        'product_name.required' => 'Please Give product_name',
        'image.required' => 'Please upload image',
        ]);
        try{
            $image = $request->file('image');
            $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(280,320)->save('uploads/mtrs-food/'.$name_gen);
            $save_url = 'uploads/mtrs-food/'.$name_gen;

            MtrsFood::insert([
                'product_name' => $request->product_name,
                'image' => $save_url,
            ]);

        return redirect()->back()->with('insert', 'Data has been insert successfully');
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Data has been insert Fail..!');
        }
        
    }

    public function foodEdit($id){
        $mtrsFood = MtrsFood::find($id);
        return view('admin.projects.mtrs-food.edit', compact('mtrsFood'));
    }

    public function foodUpdate(Request $request)
    {
        $id = $request->id;
        $old_image = $request->old_image;
        
        if ($request->file('image')) {
                unlink(($old_image));

                $image = $request->file('image');
                $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
                Image::make($image)->resize(300,350)->save('uploads/mtrs-food/'.$name_gen);
                $save_url = 'uploads/mtrs-food/'.$name_gen;

                MtrsFood::find($id)->update([
                    'product_name' => $request->product_name,
                    'image' => $save_url,
                ]);
                return redirect()->route('mtrs-food');
            }else{
                MtrsFood::find($id)->update([
                    'product_name' => $request->product_name,
                ]);
                return redirect()->route('mtrs-food');
            }
    }

    public function foodDelete($id){
        MtrsFood::find($id)->delete();
        return redirect()->back();
    }
    
}
