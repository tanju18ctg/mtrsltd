<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\About;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class AboutUsController extends Controller
{
    // index page

    public function index(){
        $abouts = About::latest()->get();
        return view('admin.about-us.index', compact('abouts'));
    }

    // store functionality

    public function store(Request $request){


        $request->validate([
            'title' => 'required|min:4',
            'description' => 'required',
            'profile' => 'required',
            'name' => 'required',
            'designation' => 'required',
        ],
        [
        'title.required' => 'Please Give title',
        'description.required' => 'Please Give description',
        'profile.required' => 'Please upload profile',
        'name.required' => 'Please Give name',
        'designation.required' => 'Please Give designation',
        ]);

        try{
        $image = $request->file('profile');
        $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(280,320)->save('uploads/about/'.$name_gen);
        $save_url = 'uploads/about/'.$name_gen;

        About::insert([
            'title' => $request->title,
            'description' => $request->description,
            'profile' => $save_url,
            'name' => $request->name,
            'designation' => $request->designation,
            'created_at' => Carbon::now()
        ]);
        return redirect()->back()->with('insert', 'Data has been insert successfully');
        }catch (\Exception $e) {
            return redirect()->back()->with('error', 'Data has been insert Fail..!');
        }
    }

    public function edit($id){
        $about = About::findOrFail($id);
        return view('admin.about-us.edit', compact('about'));
    }

    public function update(Request $request){
        $id = $request->id;
        $old_image = $request->old_image;

        if ($request->file('profile')) {
            unlink(($old_image));

            $image = $request->file('profile');
            $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(300,350)->save('uploads/about/'.$name_gen);
            $save_url = 'uploads/about/'.$name_gen;

            About::findOrFail($id)->update([
                'title' => $request->title,
                'description' => $request->description,
                'profile' => $save_url,
                'name' => $request->name,
                'designation' => $request->designation,
                'updated_at' => Carbon::now()
            ]);
            return redirect()->route('about-us');
        }else{
            About::findOrFail($id)->update([
                'title' => $request->title,
                'description' => $request->description,
                'name' => $request->name,
                'designation' => $request->designation,
            ]);
            return redirect()->route('about-us');
    }
}

    public function delete($id){
        About::findOrFail($id)->delete();
        return redirect()->back();
    }
}
