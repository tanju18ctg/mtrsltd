<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    //product index

    public function index(){
       $products = Product::latest()->get();
        return view('admin.product.index', compact('products'));
    }

    //product store

    public function store(Request $request){

        $request->validate([
            'product_name' => 'required|min:4',
            'product_image' => 'required',
        ],
    [
        'product_name.required' => 'Please Give product name',
        'product_image.required' => 'Please Upload Product Image',
    ]
    );

    try{
        $image = $request->file('product_image');
        $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
        Image::make($image)->resize(300,350)->save('uploads/product/'.$name_gen);
        $save_url = 'uploads/product/'.$name_gen;

        Product::insert([
            'product_name' => $request->product_name,
            'product_image' => $save_url
        ]);
        return redirect()->back()->with('insert', 'Data has been insert successfully');
    } catch (\Exception $e) {
        return redirect()->back()->with('error', 'Data has been insert Fail..!');
    }
        
    }


    public function edit($product_id){
 
        $product = Product::find($product_id);

       return view('admin.product.edit', compact('product'));
    }



    public function update(Request $request)
    {

        $request->validate([
            'product_name' => 'required|min:4',
        ],
    [
        'product_name.required' => 'Please Give product name',
    ]
    );

    try{
        $product_id = $request->product_id;
        $old_image = $request->old_image;

        if ($request->file('product_image')) {
            unlink(($old_image));
            $image = $request->file('product_image');
            $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(300,350)->save('uploads/product/'.$name_gen);
            $save_url = 'uploads/product/'.$name_gen;

            Product::findOrFail($product_id)->update([
                'product_name' => $request->product_name,
                'product_image' => $save_url
            ]);
            return redirect()->route('products');
    }else{
            Product::findOrFail($product_id)->update([
                'product_name' => $request->product_name,
            ]);
            return redirect()->route('products');
    }
}
catch(\Exception $e){
        return redirect()->back()->with('error', 'Data has been update Fail..!');
    }
        
      
    }



    public function delete($product_id)
    {
        Product::findOrFail($product_id)->delete();
        return redirect()->back();
    }

}
