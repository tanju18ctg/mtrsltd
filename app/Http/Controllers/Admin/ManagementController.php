<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Management;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class ManagementController extends Controller
{
    //index page
    public function index(){
      $managements = Management::latest()->get();
        return view('admin.management.index', compact('managements'));
    }



    // store page
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|min:4',
            'designation' => 'required',
            'image' => 'required',
        ],
        [
        'name.required' => 'Please Give name',
        'designation.required' => 'Please Give designation',
        'image.required' => 'Please upload image',
        ]);
        try{
            $image = $request->file('image');
            $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(280,320)->save('uploads/management/'.$name_gen);
            $save_url = 'uploads/management/'.$name_gen;

            Management::insert([
                'name' => $request->name,
                'designation' => $request->designation,
                'image' => $save_url,
            ]);
        return redirect()->back()->with('insert', 'Data has been insert successfully');
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Data has been insert Fail..!');
        }
    }

    public function edit($id){
        $management = Management::find($id);
        return view('admin.management.edit', compact('management'));
    }

    public function update(Request $request)
    {
        try{
            $id = $request->id;
            $old_image = $request->old_image;

            if ($request->file('image')) {
                unlink(($old_image));
    
                $image = $request->file('image');
                $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
                Image::make($image)->resize(300,350)->save('uploads/management/'.$name_gen);
                $save_url = 'uploads/management/'.$name_gen;
        
                Management::findOrFail($id)->update([
                    'name' => $request->name,
                    'designation' => $request->designation,
                    'image' => $save_url,
                ]);
                return redirect()->route('management'); 
        }else{
            Management::findOrFail($id)->update([
                'name' => $request->name,
                'designation' => $request->designation,
            ]);
            return redirect()->route('management'); 
        }
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Data has been update Fail..!');
        }
    }

    public function delete($id){
        Management::find($id)->delete();
        return redirect()->back();
    }


}
