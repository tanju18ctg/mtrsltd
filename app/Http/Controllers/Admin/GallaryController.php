<?php

namespace App\Http\Controllers\Admin;

use App\Models\Gallary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class GallaryController extends Controller
{
    //index page

    public function index(){
        $gallaries = Gallary::latest()->get();
        return view('admin.gallary.index', compact('gallaries'));
    }

    public function store(Request $request){

        $request->validate([
            'title' => 'required|min:4',
            'gallary_image' => 'required',
        ],
        [
        'title.required' => 'Please Give title',
        'gallary_image.required' => 'Please upload gallary_image',
        ]);

        try{
            $image = $request->file('gallary_image');
            $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(166,110)->save('uploads/gallary/'.$name_gen);
            $save_url = 'uploads/gallary/'.$name_gen;

            Gallary::insert([
                'title' => $request->title,
                'gallary_image' => $save_url
            ]);

            return redirect()->back()->with('insert', 'Data has been insert successfully');
        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Data has been insert Fail..!');
        }
        
    }

    public function edit($id){
        $gallary = Gallary::findOrFail($id);
        return view('admin.gallary.edit', compact('gallary'));
    }

    public function update(Request $request){
        $id = $request->id;
        $old_image = $request->old_image;
        if ($request->file('gallary_image')) 
        {
            unlink(($old_image));
            $image = $request->file('gallary_image');
            $name_gen=hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(300,350)->save('uploads/gallary/'.$name_gen);
            $save_url = 'uploads/gallary/'.$name_gen;

            Gallary::findOrFail($id)->update([
                'title' => $request->title,
                'gallary_image' => $save_url
            ]);
            return redirect()->route('gallary');
        }else{
            Gallary::findOrFail($id)->update([
                'title' => $request->title,
            ]);
            return redirect()->route('gallary');
        }
    }

    public function delete($id)
    {
        Gallary::findOrFail($id)->delete();
        return redirect()->back();
    }

}
